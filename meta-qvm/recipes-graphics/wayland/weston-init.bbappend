FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

USERADD_PARAM:${PN} = "--home /home/weston --shell /bin/sh --user-group -G audio,video,input,render,wayland weston"