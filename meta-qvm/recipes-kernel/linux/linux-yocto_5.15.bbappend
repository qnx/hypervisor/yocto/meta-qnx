FILESEXTRAPATHS:prepend := "${THISDIR}/${MACHINE}:"

PR := "${PR}.1"

# This gets the machine-specific kernel settings.  This file must exist in the ${MACHINE} subdirectory.
include ${MACHINE}/linux-yocto_5.15.inc
