# qvmaarch64-specific settings
#
# qvmaarch64 is based on qemuarm64 with a few tweaks

COMPATIBLE_MACHINE ?= "(^$)"
COMPATIBLE_MACHINE:armv7a = "(.*)"
COMPATIBLE_MACHINE:armv7ve = "(.*)"
COMPATIBLE_MACHINE:aarch64 = "(.*)"

KBRANCH_qvmaarch64 ?= "v5.15/standard/qemuarm64"

SRC_URI += " file://defconfig \
             file://virtio-snd.cfg "

#  Use the Linux revision that is recommended for qemuarm64 machine
SRCREV_machine_pn-linux-yocto_qvmaarch64 ?= "${SRCREV_machine_qemuarm64}"
SRCREV_meta_pn-linux-yocto_qvmaarch64 ?= "${AUTOREV}"
