SUMMARY = "Android-5.10 virtio-video external Linux kernel module"
DESCRIPTION = "${SUMMARY}"
LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-2.0-only;md5=801f80980d171dd6425610833a22dbe6"

SRCBRANCH = "trout-android12-5.10"

inherit module

## Googlesource (original)
SRC_URI = "git://android.googlesource.com/kernel/common-modules/virtual-device/;protocol=https;branch=${SRCBRANCH};name=gs \
           file://git/virtio_video/COPYING \
           file://git/virtio_video/Makefile \
           file://0001-Update-include-path.patch;patchdir=.. \
           file://0002-Apply-QNX-changes.patch;patchdir=.. \
           file://0003-Input-v4l2-functions.patch;patchdir=.. \
           file://0004-Multiple-opens.patch;patchdir=.. \
           "

SRCREV = "16756c67b99247e98777b0aaceb97dbd0ad60660"

## Depend on Linux kernel modules being built first.
DEPENDS = " virtual/kernel "

S = "${WORKDIR}/git/virtio_video"

IMAGE_INSTALL:append = "kernel-module-virtio-video"

RPROVIDES:${PN} += "kernel-module-virtio-video"
