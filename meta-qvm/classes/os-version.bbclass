#
# Writes build information to target filesystem on /etc/os.version
#
# Copyright (C) 2019 BlackBerry
#
# Licensed under the MIT license, see COPYING.MIT for details
#
# Usage: add INHERIT += "os-version" to your conf file
#

IMAGE_BUILDINFO_VARS = "DISTRO DISTRO_VERSION MACHINE"
IMAGE_BUILDINFO_FILE = "${sysconfdir}/os.version"

inherit image-buildinfo

python gen_os_version() {
    from datetime import datetime
    
    IMAGE_DATE = "${@time.strftime(\"%c %Z\")}" # getting local timezone problematic...
    d.setVar('IMAGE_DATE', IMAGE_DATE)

    # Store the extra information in the bitbake data store  
    d.appendVar('IMAGE_BUILDINFO_VARS', ' IMAGE_NAME IMAGE_DATE USER ' )

    if d.getVar('QNX_PRODUCT'):
        d.appendVar('IMAGE_BUILDINFO_VARS', ' QNX_PRODUCT ' )

    buildinfo(d)
}

IMAGE_PREPROCESS_COMMAND += "gen_os_version;"
