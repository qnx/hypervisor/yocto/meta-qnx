SUMMARY = "QNX extra assets"

LICENSE = "CLOSED"
LIC_FILES_CHKSUM = ""

SRC_URI = " file://media16x16.png \
            file://record16x16.png \
            file://triangle16x16.png \
            file://camera16x16.png "

S = "${WORKDIR}"

do_install(){
    install -d ${D}${datadir}/icons/QNX
    install -m 0755 ${WORKDIR}/*.png ${D}${datadir}/icons/QNX
}

FILES:${PN} += "${datadir}/icons/QNX/*.png"
