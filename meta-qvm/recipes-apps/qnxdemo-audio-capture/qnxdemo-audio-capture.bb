DESCRIPTION = "Simple GTK 3.0 Hello World Application"
LICENSE = "CLOSED"
SRC_URI = "file://main.c file://header.h file://playback.c file://record.c"

S = "${WORKDIR}"

DEPENDS = "gtk+3 gstreamer1.0"

inherit pkgconfig

do_compile() {
	${CC} ${LDFLAGS} main.c playback.c record.c -o qnxdemo-audio-capture `pkg-config --cflags --libs gtk+-3.0 gstreamer-1.0`
}

do_install() {
	install -d ${D}${bindir}
	install -m 0755 qnxdemo-audio-capture ${D}${bindir}
}

FILES_${PN} += "${bindir}/qnxdemo-audio-capture"

