#include "header.h"

int playback_setup(CustomData *data) {
    GstBus *bus;

    /* Create the elements */
    data->playbin = gst_element_factory_make ("playbin", "playbin");

    if (!data->playbin ) {
        g_printerr ("Not all elements could be created.\n");
        return -1;
    }

    /* Instruct the bus to emit signals for each received message, and connect to the interesting signals */
    bus = gst_element_get_bus (data->playbin);
    gst_bus_add_signal_watch (bus);
    g_signal_connect (G_OBJECT (bus), "message::error", (GCallback)playback_error_cb, data);
    g_signal_connect (G_OBJECT (bus), "message::eos", (GCallback)playback_eos_cb, data);
    g_signal_connect (G_OBJECT (bus), "message::state-changed", (GCallback)playback_state_changed_cb, data);
    gst_object_unref (bus);

    return 0;
}

int playback_start(CustomData *data) {
    g_object_set (data->playbin, "uri", PLAYBACK_PATH, NULL);

    gboolean ret = gst_element_set_state (data->playbin, GST_STATE_PLAYING);
    if (ret == GST_STATE_CHANGE_FAILURE) {
		g_printerr ("Playback: Unable to set the pipeline to the playing state.\n");
		return -1;
	}
    return 0;
}

int playback_stop(CustomData *data) {
    if (data->play_state != GST_STATE_READY) {
        gboolean ret = gst_element_set_state (data->playbin, GST_STATE_READY);
        if (ret == GST_STATE_CHANGE_FAILURE) {
            g_printerr ("Playback: Unable to set the pipeline to the ready state.\n");
            return -1;
        }
    }
    return 0;
}

void playback_cleanup(CustomData *data) {
    gst_element_set_state (data->playbin, GST_STATE_NULL);
    gst_object_unref (data->playbin);
}

/* This function is called when an error message is posted on the bus */
static void playback_error_cb (GstBus *bus, GstMessage *msg, CustomData *data) {
  GError *err;
  gchar *debug_info;

  /* Print error details on the screen */
  gst_message_parse_error (msg, &err, &debug_info);
  g_printerr ("Error received from element %s: %s\n", GST_OBJECT_NAME (msg->src), err->message);
  g_printerr ("Debugging information: %s\n", debug_info ? debug_info : "none");
  g_clear_error (&err);
  g_free (debug_info);

  /* Set the pipeline to READY (which stops playback) */
  gst_element_set_state (data->playbin, GST_STATE_READY);
}

/* This function is called when an End-Of-Stream message is posted on the bus.
 * We just set the pipeline to READY (which stops playback) */
static void playback_eos_cb (GstBus *bus, GstMessage *msg, CustomData *data) {
  g_print ("End-Of-Stream reached.\n");
  gst_element_set_state (data->playbin, GST_STATE_READY);
}

/* This function is called when the pipeline changes states. We use it to
 * keep track of the current state. */
static void playback_state_changed_cb (GstBus *bus, GstMessage *msg, CustomData *data) {
  GstState old_state, new_state, pending_state;
  gst_message_parse_state_changed (msg, &old_state, &new_state, &pending_state);
  if (GST_MESSAGE_SRC (msg) == GST_OBJECT (data->playbin)) {
    data->play_state = new_state;
    g_print ("Playback state set to %s\n", gst_element_state_get_name (new_state));
    if (old_state == GST_STATE_READY && new_state == GST_STATE_PAUSED) {
      /* For extra responsiveness, we refresh the GUI as soon as we reach the PAUSED state */
      refresh_ui (data);
    }
  }
}