#ifndef __HEADER_H
#define __HEADER_H

#include <gtk/gtk.h>
#include <gst/gst.h>
#include <gdk/gdk.h>

/* Structure to contain all our information, so we can pass it around */
typedef struct _CustomData {
    GstElement *playbin;            /* Playback pipeline */
    GstElement *recwav;             /* Recording pipeline */

    GtkWidget *slider;              /* Slider widget to keep track of current position */
    GtkWidget *streams_list;        /* Text widget to display info about the streams */
    gulong slider_update_signal_id; /* Signal ID for the slider update signal */

    GstState play_state;            /* Current state of the playback pipeline */
    GstState rec_state;             /* Current state of the recording pipeline */
    gint64 duration;                /* Duration of the clip, in nanoseconds */

    GtkWidget *play_button;
    GtkWidget *record_button;
    GtkWidget *stop_button;
} CustomData;

static const char RECORDING_PATH[] = "/var/tmp/rec.wav";
static const char PLAYBACK_PATH[] = "file:///var/tmp/rec.wav";

int playback_setup(CustomData *data);
int playback_start(CustomData *data);
int playback_stop(CustomData *data);
void playback_cleanup(CustomData *data);
static void playback_eos_cb (GstBus *bus, GstMessage *msg, CustomData *data);
static void playback_error_cb (GstBus *bus, GstMessage *msg, CustomData *data);
static void playback_state_changed_cb (GstBus *bus, GstMessage *msg, CustomData *data);

int record_setup(CustomData *data);
int record_start(CustomData* data);
int record_stop(CustomData* data);
void record_cleanup(CustomData *data);
static void record_eos_cb (GstBus *bus, GstMessage *msg, CustomData *data);
static void record_error_cb (GstBus *bus, GstMessage *msg, CustomData *data);
static void record_state_changed_cb (GstBus *bus, GstMessage *msg, CustomData *data);

gboolean refresh_ui (CustomData *data);

#endif