#include "header.h"

static gboolean timerfun(GstElement *pipeline);

int record_setup(CustomData *data) {
    GstElement *audio_source, *wav_enc, *filesink; 
    GstBus *bus;
    guint bus_watch_id;
    GstCaps *caps;
    gboolean ret;

    /* Elements creation */
    data->recwav = gst_pipeline_new ("audio_stream");
    audio_source = gst_element_factory_make ("alsasrc", "audio_source");
    wav_enc      = gst_element_factory_make ("wavenc", "wav_encoder");
    filesink     = gst_element_factory_make ("filesink", "file_sink");

    if (!data->recwav || !audio_source || !audio_source || !filesink) {
        g_printerr ("record: failed to create\n");
        return -1;
    }

    /* Sets properties on source and sink */
    g_object_set (G_OBJECT (filesink), "location", RECORDING_PATH, NULL);

    bus = gst_element_get_bus (data->recwav);
    gst_bus_add_signal_watch (bus);
    g_signal_connect (G_OBJECT (bus), "message::error", (GCallback)record_error_cb, data);
    g_signal_connect (G_OBJECT (bus), "message::eos", (GCallback)record_eos_cb, data);
    g_signal_connect (G_OBJECT (bus), "message::state-changed", (GCallback)record_state_changed_cb, data);
    gst_object_unref (bus);

	/* Add elements to a bin */
    gst_bin_add_many (GST_BIN(data->recwav), audio_source, wav_enc, filesink, NULL);

    ret = gst_element_link(audio_source, wav_enc);
    if (!ret) {
        g_print ("audio_source and wav encoder couldn't be linked\n");
        gst_caps_unref (caps);
        return FALSE;
    }
    
    ret = gst_element_link(wav_enc, filesink);
    if (!ret) {
        g_print ("wave encoder and file sink couldn't be linked\n");
        return -1;
    }
    return 0;
}

int record_start(CustomData* data) {
    // Check if we previously recorded. If so, delete it so we don't append to the file
    g_autoptr(GFile) file = g_file_new_for_path (RECORDING_PATH);
	  if (g_file_query_exists (file, NULL)) {
        g_print ("Deleting %s\n", RECORDING_PATH);
	      g_file_delete(file, NULL, NULL);

        // Re-create pipeline
        gst_element_set_state (data->recwav, GST_STATE_NULL);
        gst_object_unref (GST_OBJECT (data->recwav));
        record_setup(data);
    }

    gboolean ret = gst_element_set_state (data->recwav, GST_STATE_PLAYING);
    if (ret == GST_STATE_CHANGE_FAILURE) {
		g_printerr ("Record: Unable to set the pipeline to the playing state.\n");
		return -1;
	}

	return 0;
}

int record_stop(CustomData *data) {
    /*if (data->rec_state != GST_STATE_READY) {
        gboolean ret = gst_element_set_state (data->recwav, GST_STATE_READY);
        if (ret == GST_STATE_CHANGE_FAILURE) {
            g_printerr ("Record: Unable to set the pipeline to the ready state.\n");
            return -1;
        }
    }*/
    gst_element_send_event(data->recwav, gst_event_new_eos());
    return 0;
}

void record_cleanup(CustomData *data) {
    gst_element_set_state (data->recwav, GST_STATE_NULL);
    gst_object_unref (GST_OBJECT (data->recwav));
}

/* This function is called when an error message is posted on the bus */
static void record_error_cb (GstBus *bus, GstMessage *msg, CustomData *data) {
  GError *err;
  gchar *debug_info;

  /* Print error details on the screen */
  gst_message_parse_error (msg, &err, &debug_info);
  g_printerr ("Error received from element %s: %s\n", GST_OBJECT_NAME (msg->src), err->message);
  g_printerr ("Debugging information: %s\n", debug_info ? debug_info : "none");
  g_clear_error (&err);
  g_free (debug_info);

  /* Set the pipeline to READY (which stops recording) */
  gst_element_set_state (data->recwav, GST_STATE_READY);
}

/* This function is called when an End-Of-Stream message is posted on the bus.
 * We just set the pipeline to READY (which stops recording) */
static void record_eos_cb (GstBus *bus, GstMessage *msg, CustomData *data) {
  g_print ("End-Of-Stream reached.\n");
  gst_element_set_state (data->recwav, GST_STATE_READY);
}

/* This function is called when the pipeline changes states. We use it to
 * keep track of the current state. */
static void record_state_changed_cb (GstBus *bus, GstMessage *msg, CustomData *data) {
  GstState old_state, new_state, pending_state;
  gst_message_parse_state_changed (msg, &old_state, &new_state, &pending_state);
  if (GST_MESSAGE_SRC (msg) == GST_OBJECT (data->recwav)) {
    data->rec_state = new_state;
    g_print ("Record state set to %s\n", gst_element_state_get_name (new_state));
    if (old_state == GST_STATE_READY && new_state == GST_STATE_PAUSED) {
      /* For extra responsiveness, we refresh the GUI as soon as we reach the PAUSED state */
      refresh_ui (data);
    }
  }
}

static gboolean timerfun(GstElement *pipeline)
{
	
	return FALSE;
}