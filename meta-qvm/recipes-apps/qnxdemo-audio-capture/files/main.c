#include "header.h"

#include <string.h>

static void update_button(GtkWidget *button, gboolean enabled) {
    gtk_widget_set_sensitive(button, enabled);
    gtk_widget_set_opacity(button, enabled == TRUE ? 1 : 0.5);
}

/* This function is called when the PLAY button is clicked */
static void play_cb (GtkWidget *button, CustomData *data) {
    // Disable the button since we are starting playback
    update_button(button, FALSE);
    playback_start(data);
}

/* This function is called when the PAUSE button is clicked */
static void record_cb (GtkWidget *button, CustomData *data) {
    // Disable the button since we are starting recording
    update_button(button, FALSE);
    record_start(data);
}

/* This function is called when the STOP button is clicked */
static void stop_cb (GtkWidget *button, CustomData *data) {
    // Since we're not playing or recording anymore, disable the stop button
    update_button(data->stop_button, FALSE);

    playback_stop(data);
    record_stop(data);
}

/* This function is called when the main window is closed */
static void delete_event_cb (GtkWidget *widget, GdkEvent *event, CustomData *data) {
    stop_cb (NULL, data);
    gtk_main_quit ();
}

/* This creates all the GTK+ widgets that compose our application, and registers the callbacks */
static void create_ui (CustomData *data) {
    GtkWidget *main_window;  /* The uppermost window, containing all other windows */
    GtkWidget *controls;     /* HBox to hold the buttons */

    main_window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    g_signal_connect (G_OBJECT (main_window), "delete-event", G_CALLBACK (delete_event_cb), data);

    data->play_button = gtk_button_new();
    gtk_button_set_label(GTK_BUTTON(data->play_button), "Play");
    gtk_widget_set_size_request(data->play_button, 100, 50);
    update_button(data->play_button, FALSE);
    g_signal_connect (G_OBJECT (data->play_button), "clicked", G_CALLBACK (play_cb), data);

    data->record_button = gtk_button_new();
    gtk_button_set_label(GTK_BUTTON(data->record_button), "Record");
    gtk_widget_set_size_request(data->record_button, 100, 50);
    update_button(data->record_button, FALSE);
    g_signal_connect (G_OBJECT (data->record_button), "clicked", G_CALLBACK (record_cb), data);

    data->stop_button = gtk_button_new();
    gtk_button_set_label(GTK_BUTTON(data->stop_button), "Stop");
    gtk_widget_set_size_request(data->stop_button, 100, 50);
    update_button(data->stop_button, FALSE);
    g_signal_connect (G_OBJECT (data->stop_button), "clicked", G_CALLBACK (stop_cb), data);

    controls = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 0);
    gtk_box_pack_start (GTK_BOX (controls), data->play_button, FALSE, FALSE, 2);
    gtk_box_pack_start (GTK_BOX (controls), data->record_button, FALSE, FALSE, 2);
    gtk_box_pack_start (GTK_BOX (controls), data->stop_button, FALSE, FALSE, 2);
    gtk_box_set_homogeneous(GTK_BOX (controls), TRUE);

    gtk_container_add (GTK_CONTAINER (main_window), controls);
    gtk_window_set_default_size (GTK_WINDOW (main_window), 400, 100);

    gtk_widget_show_all (main_window);
}

gboolean refresh_ui (CustomData *data) {
    gint64 current = -1;

    // If we're done playing/recording, enable the play/record buttons
    if (data->play_state == GST_STATE_PLAYING || data->rec_state == GST_STATE_PLAYING) {
        update_button(data->play_button, FALSE);
        update_button(data->record_button, FALSE);
        update_button(data->stop_button, TRUE);
    }
    else {
        // If there is no recorded file, then disable the play button
        g_autoptr(GFile) file = g_file_new_for_path (RECORDING_PATH);
        if (g_file_query_exists (file, NULL)) {
            update_button(data->play_button, TRUE);
        }
        else {
            update_button(data->play_button, FALSE);
        }
        update_button(data->record_button, TRUE);
        update_button(data->stop_button, FALSE);
    }

    return TRUE;
}

int main(int argc, char *argv[]) {
    CustomData data;
    
    gtk_init (&argc, &argv);
    gst_init (&argc, &argv);

    memset (&data, 0, sizeof (data));
    data.duration = GST_CLOCK_TIME_NONE;
    
    // Setup playback
    if (playback_setup(&data) == -1) {
        return -1;
    }

    // Setup record
    if (record_setup(&data) == -1) {
        return -1;
    }

    /* Create the GUI */
    create_ui (&data);

    /* Register a function that GLib will call every second */
    g_timeout_add_seconds (1, (GSourceFunc)refresh_ui, &data);

    /* Start the GTK main loop. We will not regain control until gtk_main_quit is called. */
    gtk_main ();

    // Cleanup
    playback_cleanup(&data);
    record_cleanup(&data);

    return 0;
}