#@TYPE: Machine
#@NAME: qvmaarch64
#@DESCRIPTION: Machine configuration for qvmaarch64

PREFERRED_PROVIDER_virtual/xserver ?= "xserver-xorg"
PREFERRED_PROVIDER_virtual/libgl ?= "mesa"
PREFERRED_PROVIDER_virtual/libgles1 ?= "mesa"
PREFERRED_PROVIDER_virtual/libgles2 ?= "mesa"

PREFERRED_PROVIDER_virtual/kernel ?= "linux-yocto"
PREFERRED_VERSION_linux-yocto ?= "6.1%"

XSERVER = "xserver-xorg \
 ${@bb.utils.contains('DISTRO_FEATURES', 'opengl', 'mesa-driver-swrast xserver-xorg-extension-glx', '', d)} \
 xf86-video-modesetting \
 xserver-xorg-module-libint10 \
 "

require conf/machine/include/arm/armv8a/tune-cortexa57.inc
require conf/machine/include/qemu.inc

KERNEL_IMAGETYPE = "Image"

SERIAL_CONSOLES ?= "115200;ttyAMA0 115200;hvc0"
SERIAL_CONSOLES_CHECK = "${SERIAL_CONSOLES}"

# For runqemu
QB_SYSTEM_NAME = "qemu-system-aarch64"
QB_MEM = "-m 512"
QB_MACHINE = "-machine virt"
QB_CPU = "-cpu cortex-a57"
QB_SMP = "-smp 4"
QB_CPU_KVM = "-cpu host -machine gic-version=3"
# For graphics to work we need to define the VGA device as well as the necessary USB devices
QB_GRAPHICS = "-device virtio-gpu-gl-pci"
# Add the 'virtio-rng-pci' device otherwise the guest may run out of entropy
QB_OPT_APPEND = "-device qemu-xhci -device usb-tablet -device usb-kbd"
QB_OPT_APPEND += " -device virtio-rng-pci -monitor null "
QB_KERNEL_CMDLINE_APPEND = "console=ttyAMA0,115200"
# Virtio Networking support
QB_TAP_OPT = "-netdev tap,id=net0,ifname=@TAP@,script=no,downscript=no"
QB_NETWORK_DEVICE = "-device virtio-net-device,netdev=net0,mac=@MAC@"
# Virtio block device
QB_ROOTFS_OPT = "-drive id=disk0,file=@ROOTFS@,if=none,format=raw -device virtio-blk-device,drive=disk0"
# Virtio serial console
QB_SERIAL_OPT = "-device virtio-serial-device -chardev null,id=virtcon -device virtconsole,chardev=virtcon"
QB_TCPSERIAL_OPT = "-device virtio-serial-device -chardev socket,id=virtcon,port=@PORT@,host=127.0.0.1 -device virtconsole,chardev=virtcon"

#MACHINE_EXTRA_RRECOMMENDS += "virtio-audio"
