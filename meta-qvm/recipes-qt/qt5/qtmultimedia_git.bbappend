PACKAGECONFIG:append = " examples gstreamer alsa "

DISTRO_FEATURES:append = " \
  eglfs \
  linuxfb \
  dbus \
"

do_install:append() {
    # Install the qtmultimedia examples
    install -d ${D}/opt/qt5camera/
    install -m 0755 ${D}/usr/share/examples/multimediawidgets/camera/camera ${D}/opt/qt5camera/
}
FILES:${PN} += "/opt/qt5camera/camera"
